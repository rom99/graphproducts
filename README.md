Hastily prepared demo of using Neo4j to represent a graph of CAD parts and assemblies.  Provides a very basic UI for plugging in queries and navigating the graph, displaying images of parts or assemblies with their nearest neighbours in the graph.  Allows manual entry of Neo4j queries and displays results in a simple REPL-style output area.

You *should* just be able to drop this into Eclipse and go...

Hang on, I just realised there are some references that are required:

- Neo4j Community 1.8