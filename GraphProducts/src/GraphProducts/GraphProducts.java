package GraphProducts;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.index.Index;
import org.neo4j.kernel.impl.util.FileUtils;
import org.neo4j.tooling.GlobalGraphOperations;

public class GraphProducts extends JFrame {
	
	/**
	 *  Defines the relationship types 
	 */
	public static enum RelTypes implements RelationshipType
	{
	    COMPRISED_OF,
	    IS_A,
	    LOOKS_LIKE,
	    MIRRORS	    
	}
	
	private final String DB_PATH = this.getClass().getResource("res/db").getPath();
	private static final String PARTS_FILE = "res/parts.txt";
	private final File XML_DIR = new File( this.getClass().getResource("res/xml").getPath() );

	private GraphDatabaseService graphDb;
	private Index<Node> nodeIndex;
	private GraphViewer viewer;
	private JTextArea textArea;
	private JScrollPane scroller;
	private JTextField textField;
	private JButton button;
	private String instruction;
	private SampleQueries queries;
	
	Node partNode;
	Node assemblyNode;
	Node imageNode;
	
	/** Main method */
	public static void main(String[] args) {
		GraphProducts gProds = new GraphProducts();
		gProds.clearDb(); // deletes the database to start again
		gProds.initDb();
		gProds.populateDB();
		
		gProds.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		gProds.setPreferredSize( new Dimension(900, 700) );
		gProds.setResizable(false);
		gProds.createGUI();		
		gProds.printMessage();
		gProds.setVisible(true);
	}
	
	/** Create GUI */
	private void createGUI() {
		setLayout(new FlowLayout() );
		viewer = new GraphViewer(graphDb);
		textField = new JTextField("Enter instruction");
		textField.addFocusListener(new FocusListener() {
			public void focusLost(FocusEvent fe) {
			}
			public void focusGained(FocusEvent fe) {
				textField.setText("");
				textField.removeFocusListener(this);
			}
		});
		textField.setPreferredSize( new Dimension(880, 25));
		textField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				instruction = textField.getText();
				doInstruction();
			}
		});
		textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setFont(new Font("Courier", Font.PLAIN, 11) );
		doInstructionMessage();
		scroller = new JScrollPane(textArea);
		scroller.setPreferredSize(new Dimension(880, 300) );		
		this.add(viewer);
		this.add(scroller);
		this.add(textField);
		pack();
	}
	
	/* Initialise the db */
	private void initDb() {
		graphDb = new GraphDatabaseFactory().newEmbeddedDatabase( DB_PATH );				
		nodeIndex = graphDb.index().forNodes( "nodes" );
		getRootNodes();
		registerShutdownHook( graphDb );
		queries = new SampleQueries(graphDb);
	}
	
	private void getRootNodes() {
		// Add 'root' nodes if they don't exist - assembly, part, image
		Transaction tx = graphDb.beginTx();
		try {
			partNode = graphDb.createNode();
			partNode.setProperty("name", "part");
			nodeIndex.add(partNode, "name", "part");

			assemblyNode = graphDb.createNode();
			assemblyNode.setProperty("name", "assembly");
			nodeIndex.add(assemblyNode, "name", "assembly");

			imageNode = graphDb.createNode();
			imageNode.setProperty("name", "image");
			nodeIndex.add(imageNode, "name", "image");
			tx.success();
		} finally {
			tx.finish();
		}
	}

	private void populateDB() {
		// FileFilter for .xml files
		FilenameFilter filenameFilter = new FilenameFilter() {
			public boolean accept(File dir, String name) {
				if (name.endsWith(".xml")) {
					return true;
				}
				return false;
			}
		};
		
		String[] filenames = XML_DIR.list(filenameFilter);
		
		XMLProcessor proc = new XMLProcessor(graphDb, nodeIndex, partNode, assemblyNode, imageNode);
		for(String filename : filenames ) {
			proc.processXML(XML_DIR+"/"+filename);
		}
	}
	
	/* Prints a message with details of the graph */
	private void printMessage() {
		GlobalGraphOperations ggo = GlobalGraphOperations.at(graphDb);
		int count = 0;
		System.out.println("\nDB STRUCTURE: ");
		for(Node node : ggo.getAllNodes() ){			
			if(node.hasProperty("name") ) {
				System.out.println( "Node" + count + ": " + node.getProperty( "name" ) );
				for(Relationship rel: node.getRelationships(Direction.OUTGOING) ) {
					if(rel.hasProperty("count") ) {
					System.out.println( "\t" + rel.getType().name() 
							+ " => " + rel.getEndNode().getProperty( "name")
							+ " count: " + rel.getProperty( "count" ) );
					}
					if(rel.isType(RelTypes.MIRRORS) ) {
						System.out.println("\t" + rel.getType().name() 
								+ " => " + rel.getEndNode().getProperty("name") );
					}
				}
				count++;
			}
		}
	}
	
	/* Shutdown the graph database */
	private void end() {
		if(graphDb != null) {
			graphDb.shutdown();
		}
		System.out.println("Graph shutdown");
	}
	
	/* Deletes the db folder */
	private void clearDb() {
		try {
			FileUtils.deleteRecursively(new File(DB_PATH));
			System.out.println("Deleted database: " + DB_PATH);
		} catch (IOException e) {
			System.err.println("Error clearing db at " + DB_PATH);
			throw new RuntimeException(e);
		}
	}
	
	/* Registers a shutdown hook for the Neo4j instance so that it
     	shuts down nicely when the VM exits (even if you "Ctrl-C" the
    	running example before it's completed) */
    private static void registerShutdownHook( final GraphDatabaseService graphDb )
    {        
        Runtime.getRuntime().addShutdownHook( new Thread()
        {
            @Override
            public void run()
            {
                graphDb.shutdown();
            }
        } );
    }
    
    private void message(String msg) {
    	textArea.setText(textArea.getText() + msg + "\n");
    }
    
    private boolean checkNodesForString() {
    	for(Node node : GlobalGraphOperations.at(graphDb).getAllNodes() ) {
			if(node.hasProperty("name") ) {
				if (instruction.equals(node.getProperty("name"))) {
					viewer.displayNode(node);
					message("Displaying node: " + node.getProperty("name"));
					return true;
				}
			}
		} 
    	return false;
    }
    
    private void doInstruction() {	
		try {
			if (instruction.equals("quit")) {
				end();
				System.out.println("Quitting application");
				System.exit(0);
			} else if (instruction.startsWith("Q ", 0)) {
				message("Trying query: "+instruction.substring(2));
				message(queries.simpleQuery(instruction.substring(2))
						.toString());
			} else if (instruction.equals("parts")) {
				message(queries.getNodesByType(partNode.getId(), "Parts" ).toString() );
			} else if (instruction.equals("assemblies")) {
				message(queries.getNodesByType(assemblyNode.getId(), "Assemblies").toString() );
			} else if(instruction.equals("nodes") ) {
				message(queries.getAllNodesByType("Nodes").toString() );
			} else if(instruction.equals("images")) {
				message(queries.getNodesByType(imageNode.getId(), "Images").toString() );
			}			
			else if (checkNodesForString()) {
				// side effects
			} else {
				message("Instruction not recognised.");
				doInstructionMessage();
			}
		} catch (Exception ex) {
			message(ex.getLocalizedMessage() );
		}
    }
    
    private void doInstructionMessage() {
    	String instructions = "\nTo query the DB, enter \"Q \" (including space) followed by a Cypher QL query.\n" +
    			"To display parts, assemblies and relationships, enter the part or assembly name.\n" +
    			"To list all parts, enter \"parts\".\n" +
    			"To list all assemblies, enter \"assemblies\".\n" +
    			"To list all nodes, enter \"nodes\".\n" +
    			"To list all images, enter \"images\".\n" +
    			"To quit, enter \"quit\".\n";
    	message(instructions);
    }
}
