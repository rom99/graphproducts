package GraphProducts;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class GraphicItem {
	protected int posX, posY, width, height, count;
	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public void setSize(int size) {
		height=size;
		width=size;
	}

	protected BufferedImage image;
	protected String name;
	
	public GraphicItem(String name, String imagename, int count) {
		this.name = name;
		this.count = count;
		if(imagename!=null) {
			try {
				image = ImageIO.read( GraphProducts.class.getResource( "res/img/"+imagename) );
				System.out.println("image read: " + imagename);
				width = image.getWidth();
				height = image.getHeight();
			} catch (IOException ioex) {
				System.err.println("Error reading image file: " + ioex.getLocalizedMessage() );
			}
		} else {
			image = null;
			width=1;
			height=1;
		}
	}
	
	public void draw(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		if(image!=null)
			g2d.drawImage(image, posX, posY, width, height, null);
		g2d.setFont(new Font( "Courier", Font.PLAIN, 11 ));
		g2d.setColor(Color.BLACK);
		g2d.drawString(name, posX, posY + height + 5);
	}	
}
