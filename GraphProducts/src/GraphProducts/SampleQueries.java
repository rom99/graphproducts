package GraphProducts;

import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.cypher.javacompat.ExecutionResult;
import org.neo4j.graphdb.GraphDatabaseService;

public class SampleQueries {
	
	private ExecutionEngine engine;
	
	public SampleQueries(GraphDatabaseService gdb) {
		engine = new ExecutionEngine(gdb);
	}
		
	public ExecutionResult simpleQuery( String query ) throws Exception {
		ExecutionResult result = engine.execute( query );
		//System.out.println( result );
		return result;
	}
	
	public ExecutionResult getNodesByType(long id, String colname) throws Exception {
		return simpleQuery( "start n=node("+id+") match n<-[:IS_A]-"+colname+" return "+colname );
	}
	
	public ExecutionResult getAllNodesByType(String colname) throws Exception {
		return simpleQuery( "start n=node(1,2,3) match n<--"+colname+" return distinct "+colname+" order by ID("+colname+")" );
	}
}
