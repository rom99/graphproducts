package GraphProducts;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class MainGraphicItem extends GraphicItem {
	
	public MainGraphicItem(String name, String imagename, int count) {
		super(name, imagename, count);
	}
	
	/* Draw the central node*/
	public void draw(Graphics g, Font font, int nameX, int nameY, int nameWidth) {
		int imageSize = 100;
		g.setColor(Color.black);
		g.setFont(font);
		g.drawImage(image, nameX - imageSize/2, nameY - imageSize /2, null);
		g.drawString(name, nameX- (imageSize - nameWidth ) /2, nameY + imageSize * 2/3 );
	}
}
