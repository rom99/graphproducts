package GraphProducts;

import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.index.Index;

/**
 * Class for processing AssemblyTree XML files
 * @author Ross Mills
 *
 */
public class XMLProcessor {
	
	private GraphDatabaseService graphDb;
	private Index<Node> nodeIndex;
	private URL xml;

	// XML element names
	private static final String TREE = "AssemblyTree";
	private static final String ASSEMBLY = "Assembly";
	private static final String PART = "Partinstance";
	private static final String MIRROR = "mirror";

	// XML attribute names
	private static final String NAME = "Name";
	private static final String COUNT = "count";
	private static final String VIS = "VisibilityStatus";
	private static final String IMG = "Image";

	private String rootAssemblyName = null;
	
	private Node partNode, assemblyNode, imageNode;
	
	/**
	 * Constructor
	 * @param filename	Filename to be processed (relative to src folder)
	 * @param graphDb	GraphDatabaseService object
	 */
	public XMLProcessor( GraphDatabaseService graphDb, Index<Node> nodeIndex, Node partNode, Node assemblyNode, Node imageNode) {
		this.graphDb = graphDb;		
		this.nodeIndex = nodeIndex;
		this.partNode = partNode;
		this.assemblyNode = assemblyNode;
		this.imageNode = imageNode;
	}

	/** 
	 * Reads an AssemblyTree xml file into the database. Does not create duplicate nodes or relationships.
	 * 
	 * @param filename
	 */
	public void processXML(String filename) {
		//this.xml = GraphProducts.class.getResource(filename);
		Transaction tx = graphDb.beginTx();
		try {
			// Get eventReader object over xml file
			XMLInputFactory inputFactory = XMLInputFactory.newInstance();
			FileInputStream in = new FileInputStream( filename );
			XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
			System.out.println("Reading xml: " + filename);

			// Iterate over xml events
			while (eventReader.hasNext()) {
				XMLEvent event = eventReader.nextEvent();

				// Deal with start elements
				if (event.isStartElement()) {
					StartElement startEl = event.asStartElement();

					// Is this the TREE element?
					// if(startEl.getName().getLocalPart().equals(TREE) ) {
					//
					// }

					// Is this an ASSEMBLY element?
					if (startEl.getName().getLocalPart().equals(ASSEMBLY)) {
						processAssemblyElement(startEl);
					}

					// Is this a PART element?
					else if (startEl.getName().getLocalPart().equals(PART)) {
						processPartElement(startEl);
					}
				}
			}
			rootAssemblyName = null; // reset rootAssemblyName for next time
			in.close();
			tx.success();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			tx.finish();
		}
	}

	/* 
	 * XML parsing helper method 
	 */
	private void processPartElement(StartElement el) {
		String partName = null;
		String imgName = null;
		Node mirror = null;
		int count = 0;

		Iterator<Attribute> atts = el.getAttributes();
		while (atts.hasNext()) {
			Attribute att = atts.next();
			String attName = att.getName().getLocalPart();
			// Is attribute the NAME attribute?
			if (attName.equals(NAME)) {
				partName = att.getValue();
			}
			// Is attribute the COUNT attribute?
			else if (attName.equals(COUNT)) {
				count = Integer.parseInt(att.getValue());
			}
			// Is attribute the Mirror attribute?
			else if (attName.equals(MIRROR)) {
				mirror = addPartNode( att.getValue() );				
			}			
			// Is attribute the VIS attribute?
			else if (attName.equals(VIS)) {
				// ignored for now
			}
			// Is attribute the IMG attribute?
			else if(attName.equals(IMG)) {
				imgName = att.getValue();
			}
		}
		
		Node node = addPartNode(partName);
		
		// Create image node and relationship to image if it exists
		if(imgName != null) {
			Node imgNode = addImgNode(imgName);
			createRelationship( node, imgNode, GraphProducts.RelTypes.LOOKS_LIKE);
		}
		
		// Create mirrors relationship if it exists
		if(mirror != null)
			createRelationship( node, mirror, GraphProducts.RelTypes.MIRRORS);
		
		// relate to rootAssembly if it exists
		if(rootAssemblyName != null) {
			Node rootNode = nodeIndex.get("name", rootAssemblyName).getSingle();		
			Relationship rel = createRelationship(rootNode, node, GraphProducts.RelTypes.COMPRISED_OF);
			if(rel != null) {
				rel.setProperty("count", count);
			}
		}
	}
	
	/* add image to graph if it doesn't exist */
	private Node addImgNode(String imgName) {
		Node node = null;
		if(nodeIndex.query("name", imgName).size() == 0) {
			node = graphDb.createNode();
			node.setProperty("name", imgName);
			nodeIndex.add(node, "name", imgName);
			node.createRelationshipTo(imageNode, GraphProducts.RelTypes.IS_A);
			
		} else {
			node = nodeIndex.get("name", imgName).getSingle();
		} 
		return node;
	}
	
	/* add partInstance to graph if it doesn't exist */ 
	private Node addPartNode(String partName) {
		Node node = null;
		if (nodeIndex.query("name", partName).size() == 0) {
			node = graphDb.createNode();
			node.setProperty("name", partName);
			nodeIndex.add(node, "name", partName);
			node.createRelationshipTo(partNode, GraphProducts.RelTypes.IS_A);
		} else {
			node = nodeIndex.get("name", partName).getSingle();
		}
		return node;
	}
	
	/* add assembly to graph if it doesn't exist */
	private Node addAssemblyNode(String name) {
		Node node = null;
		if (nodeIndex.query("name", name).size() == 0) {
			node = graphDb.createNode();
			node.setProperty("name", name);
			nodeIndex.add(node, "name", name);
			node.createRelationshipTo(assemblyNode, GraphProducts.RelTypes.IS_A);
		} else {
			node = nodeIndex.get("name", name).getSingle();
		}
		return node;
	}
	
	/* 
	 * XML parsing helper method 
	 */
	private void processAssemblyElement(StartElement el) {
		String assemblyName = null;
		String imgName = null;
		int count = 0;
		boolean rootAssembly = false;

		Iterator<Attribute> atts = el.getAttributes();
		// Get attributes and iterate over them
		while (atts.hasNext()) {
			Attribute att = atts.next();
			String attName = att.getName().getLocalPart();
			// Is attribute the NAME attribute?
			if (attName.equals(NAME)) {
				assemblyName = att.getValue();
				// set as rootAssemblyNode if it's not yet been set
				if (rootAssemblyName == null) {
					rootAssemblyName = assemblyName;
					addAssemblyNode(rootAssemblyName);
					rootAssembly = true;
				}
			}
			// Is attribute the COUNT attribute?
			else if (attName.equals(COUNT)) {
				count = Integer.parseInt(att.getValue());
			}
			// Is attribute the VIS attribute?
			else if (attName.equals(VIS)) {
				// ignored for now
			}
			// Is attribute the IMG attribute?
			else if(attName.equals(IMG)) {
				System.out.println("Found assembly Image att");
				imgName = att.getValue();
			}
		}

		// Get the root assembly node
		Node rootNode = nodeIndex.get("name", rootAssemblyName).getSingle();
		// Get the new node 
		Node node = null;
		// Create node and relationship to rootNode, if this is not the root node
		if(!rootAssembly) {
			//	Add a node if it doesn't exist in the index
			node = addAssemblyNode(assemblyName);
			// Add a relationship from root assembly node to the assembly
			Relationship rel = createRelationship(rootNode, node, GraphProducts.RelTypes.COMPRISED_OF);
			if(rel != null) 
				rel.setProperty("count", count);
		} else {
			node = rootNode;
		}
		
		if(imgName!=null) {
			System.out.println("About to add assembly img node: " + imgName);
			Node imgNode = addImgNode(imgName);
			createRelationship( node, imgNode, GraphProducts.RelTypes.LOOKS_LIKE);
		}
	}
	
	/*
	 * Creates a relationship of type relType from node1 to node2 - after checking such
	 * a relationship does not already exist. 
	 */
	private Relationship createRelationship( Node node1, Node node2, RelationshipType relType ) {
		// Lock the nodes to prevent another thread from interfering
		node1.setProperty("dummy", 0 );
		node2.setProperty("dummy", 0);
		Relationship rel = null;
		boolean relationshipExist = false;
		for (Relationship r : node1.getRelationships(relType) ) {
			if (r.getOtherNode(node1).equals(node2) ) {
				relationshipExist = true;
				break;
			}
		}
		if (!relationshipExist) {
			rel = node1.createRelationshipTo(node2, relType);
		}		
		// remove dummy property
		node1.removeProperty("dummy");
		node2.removeProperty("dummy");
		return rel;
	}
}
