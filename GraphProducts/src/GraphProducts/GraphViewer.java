package GraphProducts;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import javax.swing.JPanel;

import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.cypher.javacompat.ExecutionResult;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;

/**
 * For viewing graph structures
 * @author rossmills
 *
 */
public class GraphViewer extends JPanel {
	
	private GraphDatabaseService graphDb;
	private boolean NODE_DISPLAY = false;
	private Node node = null;
	private ArrayList<GraphicItem> pofGraphicItems = new ArrayList<GraphicItem>();
	private ArrayList<GraphicItem> cofGraphicItems = new ArrayList<GraphicItem>();
	private MainGraphicItem graphicItem;
	private ExecutionEngine engine;
	
	public GraphViewer(GraphDatabaseService gdb ) {
		this.graphDb = gdb;
		setPreferredSize(new Dimension(880, 300) );
		setBackground(Color.white);
		engine = new ExecutionEngine(graphDb);
	}
		
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		clearScreen(g);
		if(!NODE_DISPLAY) {
			drawAll(g);
		}
		if(NODE_DISPLAY) {
			if(node != null ) {
				drawKey(g);
				displayNode(g);
			}
		}
	}

	public void displayNode(Node node) {
		pofGraphicItems.clear();
		cofGraphicItems.clear();
		this.node = node;
		String imagename = null;
		if(node.hasRelationship(GraphProducts.RelTypes.LOOKS_LIKE, Direction.OUTGOING) ) {
			// get the image name from the image node, if this part/assembly node has one
			imagename = node.getSingleRelationship(
					GraphProducts.RelTypes.LOOKS_LIKE,
					Direction.OUTGOING)
						.getEndNode()
						.getProperty("name")
						.toString();
		}
		graphicItem = new MainGraphicItem(node.getProperty("name").toString(), imagename, 1 );
		createPOFItems();
		createCOFItems();
		
		NODE_DISPLAY = true;
		repaint();
	}
	
	private void drawAll(Graphics g) {
		
	}
	
	private void displayNode(Graphics g) {
		g.setColor(Color.black);
		
		String name = node.getProperty("name").toString();
		Font font = new Font("Courier", Font.PLAIN, 14);
		
		FontMetrics fmLarge = this.getFontMetrics(font);
		
		//Find x position to centre name text
		int nameX = this.getWidth()/2 - fmLarge.stringWidth(name) /2;
		int nameY = 180;		
		
		int cofPosX = nameX + this.getWidth()/4;
		int pofPosX = nameX - this.getWidth()/4;
		
		drawItems(g, cofPosX, cofGraphicItems);
		drawItems(g, pofPosX, pofGraphicItems);

		drawLines(g, this.getWidth()/2, nameY + 5);

		graphicItem.draw(g, font, this.getWidth() / 2, nameY, fmLarge.stringWidth(name) );
	}
	
	private void drawKey(Graphics g) {
		g.setColor(Color.BLUE);
		g.drawString("\"is comprised of\"", 5, this.getHeight() - 5);
		g.setColor(Color.RED);
		g.drawString("\"is part of\"", 5, this.getHeight() - 20);
		g.setColor(Color.black);
		g.drawString("Key:", 5, this.getHeight() - 40);
	}
	
	private void drawLines(Graphics g, int nameX, int nameY) {
		for(GraphicItem item : cofGraphicItems ) {
			g.setColor(Color.blue);
			int x1 = item.getPosX();
			int y1 = item.getPosY() + ( item.getHeight() / 2 );
			int xMid = ( nameX - x1 ) / 2;
			int yMid = ( nameY - y1 ) / 2;
			g.setFont(new Font("Courier", Font.ITALIC, 10));
			g.drawLine(nameX, nameY, x1, y1);
			g.drawString("count: " + item.getCount(), this.getWidth() - 105, y1);
		}
		for(GraphicItem item : pofGraphicItems ) {
			g.setColor(Color.red);
			int x1 = item.getPosX() + item.getWidth();
			int y1 = item.getPosY() + item.getHeight()/2;
			g.setFont(new Font("Courier", Font.ITALIC, 10));
			g.drawLine(nameX, nameY, x1, y1);
			g.drawString("count: " + item.getCount(), 50, y1);
		}
	}
	
	private void drawItems(Graphics g, int posX, ArrayList<GraphicItem> list) {
		int itemsSize = list.size();
		int ySpacing = 15;
		// Don't want to divide by zero!
		if(itemsSize > 0)
			ySpacing = this.getHeight() / itemsSize;
		for(int i =0; i < itemsSize; i++) {
			GraphicItem item = list.get(i);
			item.setPosX(posX);
			item.setPosY(i + (i * ySpacing) );
			item.setSize(ySpacing / 2);
			item.draw(g);
		}
	}
	
	private void createCOFItems() {
		// Get COMPRISED_OF relationships and images
		String query = "start n=node("+node.getId()+") " +
				"match n-[rel:COMPRISED_OF]->part-[?:LOOKS_LIKE]->image " +
				"return part, rel.count, image";
		ExecutionResult cofResult = engine.execute(query);
		processResults(cofResult, cofGraphicItems);
	}
	
	private void createPOFItems() {
		// Get PART_OF nodes (opposite of COMPRISED_OF) and images
		String query = "start n=node(" + node.getId() + ") "
				+ "match n<-[rel:COMPRISED_OF]-part-[?:LOOKS_LIKE]->image "
				+ "return part, rel.count, image";
		ExecutionResult pofResult = engine.execute(query);
		processResults(pofResult, pofGraphicItems);
	}
	
	private void processResults(ExecutionResult result, ArrayList<GraphicItem> list) {
		Iterator<Map<String, Object>> inNodes = result.iterator();
		// Extract properties of Assembly nodes and image Nodes
		while (inNodes.hasNext()) {
			Map<String, Object> resultMap = inNodes.next();
			Node node = (Node) resultMap.get("part");
			Node image = (Node) resultMap.get("image");
			int count = (Integer) resultMap.get("rel.count");
			String imageName = null;
			// image might be null
			if (image != null) {
				imageName = image.getProperty("name").toString();
			}
			String nodeName = node.getProperty("name").toString();
			// add new graphic item for nodes
			list.add(new GraphicItem(nodeName, imageName, count) );
		}
	}
	
	private void clearScreen(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(0, 0, this.WIDTH, this.HEIGHT);
	}
}
